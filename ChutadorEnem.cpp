#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char respostas[] = { 'A', 'B', 'C', 'D', 'E' };
	int randnumber = 0;
	srand(time(NULL));

	for (int i = 0; i < 90; i++)
	{
		randnumber = rand() % 5;
		std::cout << "Questao " << i << ": " << respostas[randnumber] << std::endl;
	}

	std::cout << "Hello World!\n";
	return 0;
}